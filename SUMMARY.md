# 目录

* [README](README.md)
* [1-Java基础](/README.md)
  + [1.1-JavaEE基础](basic/javaEEBasic.md)
  + [1.2-Java基础](basic/javaBasic.md)
  + [1.3-Java集合](basic/assemble.md)
  + [1.4-设计模式](basic/DesignModel.md)
  + [1.5-多线程](basic/nThreads.md)

  
* [2-框架](fromework/README.md)
  + [2.1-spring](framework/spring.md)
  + [2.2-springBoot](framework/springBoot.md)
  + [2.3-springCloud](framework/springCloud.md)
  + [2.4-springMVC](framework/SpringMVC.md)
  + [2.5-MyBatis](framework/MyBatis.md)

* [3-数据存储](data_storage/README.md)
  * [3.1-Redis](data_storage/Redis.md)
  * [3.2-mysql](data_storage/Mysql.md)
  * [3.3-MongoDB](data_storage/MongoDB.md)

* [4-第三方应用](third_program/README.md) 
  * [4.1-Nginx](third_program/Nginx.md)
  * [4.2-Elasticsearch](third_program/Elasticsearch.md)
  * [4.3-Dubbo](third_program/Dubbo.md)
  * [4.4-zookeeper](third_program/zookeeper.md)

 
* [5-其他](other/README.md) 
  * [5.1-并发编程](other/BingFaQuestion.md)
  * [5.2-BTA面试题](other/BTAquestion.md)
  * [5.3-BIO、AIO、NIO总结](other/BIO&NIO&AIO.md)


 
